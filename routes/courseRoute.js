const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");

// Route for creating a course
// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });

router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all" , (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving active courses
router.get("/active" , (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:courseId", (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route to archiving a course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});


module.exports = router;









// const express = require("express");
// const router = express.Router();

// const courseController = require("../controllers/courseController");

// const auth = require("../auth");

// // Route for creating a course
// // router.post("/create", (req, res) => {
// // 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// // });

// router.post("/create", auth.verify, (req, res) => {

// 	const data = {
// 		course: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
// });


// /*  S40 activity code   */


// router.patch("/:id/archive", auth.verify, (req, res) => {
// 	const data = {
// 		courseId: req.params.id,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
// });




// /*  s40 end of activity code  */




// // Route for retrieving all the courses
// router.get("/all" , (req, res) => {
// 	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
// });

// // Route for retrieving active courses
// router.get("/active" , (req, res) => {
// 	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
// });

// // Route for retrieving specific course
// router.get("/:courseId", (req, res) => {
// 	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
// });


// // Route for updating a course
// router.put("/:courseId", (req,res) => {
// 	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
// });


// module.exports = router;










